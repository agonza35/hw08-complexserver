import cherrypy
import re, json
from groceries_library import _groceries_database

class GroceriesController(object):
    def __init__(self, gdb=None):
        if gdb is None:
            self.gdb = _groceries_database()
        else:
            self.gdb = gdb
        
        self.gdb.load_groceries('groceries.dat')



    def GET_KEY(self, itemID):
        output = {'result' : 'success'}
        itemID = int(itemID)

        try:
            item = self.gdb.get_grocery(itemID)
            if item is not None:
                output['itemID'] = itemID
                output['item'] = item[0]
                output['brand'] = item[1]
                output['category'] = item[2]
                output['location'] = item[3]
                output['price'] = item[4]
            else:
                output['result'] = 'error'
                output['message'] = 'item not found'
        except Exception as ex:
                output['result'] = 'error'
                output['message'] = str(ex)
                
        return json.dumps(output)
        
        
    def GET_INDEX(self):
        output = {'result' : 'success'}
        output['items'] = []
        
        try:
            for itemID in self.gdb.get_groceries():
                item = self.gdb.get_grocery(itemID)
                ditem = {'itemID': itemID, 'item':item[0], 'brand':item[1], 
                        'category':item[2], 'location':item[3], 'price':item[4]}
                output['items'].append(ditem)
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)
    
        return json.dumps(output)
    
    def PUT_KEY(self, itemID):
        output = {'result':'success'}
        itemID = int(itemID)
        
        data = json.loads(cherrypy.request.body.read().decode('utf-8'))
        
        grocery = list()
        grocery.append(data['item'])
        grocery.append(data['brand'])
        grocery.append(data['category'])
        grocery.append(data['location'])
        grocery.append(data['price'])
    
        self.gdb.set_grocery(itemID, grocery)

        return json.dumps(output)        
        
        
    def POST_INDEX(self):
        output = {'result' : 'success'}
        data = json.loads(cherrypy.request.body.read().decode('utf-8'))
        
        try:
            groceries = sorted(list(self.gdb.get_groceries()))
            newID = int(groceries[-1]) + 1
            self.gdb.groceries_item[newID] = data['item']
            self.gdb.groceries_brand[newID] = data['brand']
            self.gdb.groceries_category[newID] = data['category']
            self.gdb.groceries_location[newID] = data['location']
            self.gdb.groceries_price[newID] = data['price']
            output['itemID'] = newID
            
        except Exception as ex:
            output['result'] = 'failure'
            output['message'] = str(ex)
            
        return json.dumps(output)
            
            
    def DELETE_KEY(self, itemID):
        output = {'result' : 'success'}
        itemID = int(itemID)
        
        try:
            self.gdb.delete_grocery(itemID)
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)
        return json.dumps(output)

        
    def DELETE_INDEX(self):
        output = {'result' : 'success'}
        
        try:
            allItems = list(self.gdb.get_groceries())
            for item in allItems:
                self.gdb.delete_grocery(item)
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)
        