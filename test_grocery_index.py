import unittest
import requests
import json

class TestGroceriesIndex(unittest.TestCase):
    
    SITE_URL = 'http://localhost:51021'
    print("testing for server: " + SITE_URL)
    GROCERY_URL = SITE_URL + '/groceries/'
    RESET_URL = SITE_URL + '/reset/'

    def reset_data(self):
        g = {}
        r = requests.put(self.RESET_URL, json.dumps(g))
        
    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

           
    def test_groceries_index_get(self):
        self.reset_data()
        r = requests.get(self.GROCERY_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        testItem = {}
        groceries = resp['items']
        for item in groceries:
            if item['itemID'] == 7:
                testItem = item


        self.assertEqual(testItem['item'], 'Horseradish')
        self.assertEqual(testItem['brand'], 'Boar’s Head Horseradish')
        self.assertEqual(testItem['category'], 'Staples & Misc.')
        self.assertEqual(testItem['location'], 'Pantry')
        self.assertEqual(testItem['price'], '4.59')

        
        
    def test_groceries_index_post(self):
        self.reset_data()

        g = {}
        g['item'] = 'Soda'
        g['brand'] = 'Coca-Cola'
        g['category'] = 'Beverages'
        g['location'] = 'Fridge'
        g['price'] = '3.35'
        r = requests.post(self.GROCERY_URL, data = json.dumps(g))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        self.assertEqual(resp['itemID'], 34)
    
        r = requests.get(self.GROCERY_URL + str(resp['itemID']))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['item'], g['item'])
        self.assertEqual(resp['brand'], g['brand'])
        self.assertEqual(resp['category'], g['category'])
        self.assertEqual(resp['location'], g['location'])
        self.assertEqual(resp['price'], g['price'])
           
        
    def test_groceries_index_delete(self):
        self.reset_data()
        
        g = {} 
        r = requests.delete(self.GROCERY_URL, data = json.dumps(g))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        
        r = requests.get(self.GROCERY_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        groceries = resp['items']
        self.assertFalse(groceries)
if __name__ == "__main__":
    unittest.main()