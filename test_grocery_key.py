from random import gammavariate
import unittest
import requests
import json

class TestGroceriesKey(unittest.TestCase):

    SITE_URL = 'http://localhost:51021'
    print("testing for server: " + SITE_URL)
    GROCERY_URL = SITE_URL + '/groceries/'
    RESET_URL = SITE_URL + '/reset/'
    
    
    def reset_data(self):
        g = {}
        r = requests.put(self.RESET_URL, data = json.dumps(g))


    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        
        except ValueError:
            return False


    def test_grocery_get_key(self):
        self.reset_data()
        itemID = 7 # test ID
        r = requests.get(self.GROCERY_URL + str(itemID))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['item'], 'Horseradish')
        self.assertEqual(resp['brand'], 'Boar’s Head Horseradish')
        self.assertEqual(resp['category'], 'Staples & Misc.')
        self.assertEqual(resp['location'], 'Pantry')
        self.assertEqual(resp['price'], '4.59')

    def test_grocery_put_key(self):
        self.reset_data()
        itemID = 21 # test ID

        r = requests.get(self.GROCERY_URL + str(itemID))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['item'], 'Apple Juice')
        self.assertEqual(resp['brand'], 'Mott’s Natural Apple Juice')
        self.assertEqual(resp['category'], 'Beverages')
        self.assertEqual(resp['location'], 'Fridge')
        self.assertEqual(resp['price'], '3.79')

        g = {}
        g['item'] = 'updated Apple Juice'
        g['brand'] = 'Clear Apple'
        g['category'] = 'Beverages'
        g['location'] = 'Fridge'
        g['price'] = '4.35'
        r = requests.put(self.GROCERY_URL + str(itemID), data = json.dumps(g))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')
        
        r = requests.get(self.GROCERY_URL + str(itemID))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['item'], g['item'])
        self.assertEqual(resp['brand'], g['brand'])
        self.assertEqual(resp['category'], g['category'])
        self.assertEqual(resp['location'], g['location'])
        self.assertEqual(resp['price'], g['price'])
         
       

    def test_grocery_delete_key(self):
        self.reset_data()
        itemID = 12 # test ID
        
        g = {}
        r = requests.delete(self.GROCERY_URL + str(itemID), data = json.dumps(g))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')
        
        r = requests.delete(self.GROCERY_URL + str(itemID))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'error')
        
        
if __name__ == "__main__":
    unittest.main()
    