import unittest
import requests
import json

class TestReset(unittest.TestCase):
    SITE_URL = 'http://localhost:51021'
    print("testing for server: " + SITE_URL)
    RESET_URL = SITE_URL + '/reset/'
    
    def test_put_reset_index(self):
        g = {}
        r = requests.put(self.RESET_URL, json.dumps(g))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        r = requests.get(self.SITE_URL + '/groceries/')
       # print(r)
        resp = json.loads(r.content.decode())
       # print(resp)
        groceries = resp['items']
        self.assertEqual(groceries[5]['item'], 'Sandwich Bread')

    def test_put_reset_key(self):
        g = {}
        r = requests.put(self.RESET_URL, json.dumps(g))

        # change item features
        itemID = 7
        g['item'] =  'Horseradish'
        g['brand'] = 'Golds Horseradish'
        g['category'] = 'Staples & Misc.'
        g['location'] = 'Pantry'
        g['price'] = '3.05'
        r = requests.put(self.SITE_URL + '/groceries/' + str(itemID), data=json.dumps(g))

        # reset back to original
        g = {}
        r = requests.put(self.RESET_URL + str(itemID), data=json.dumps(g))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        
        # check if effective
        r = requests.get(self.SITE_URL + '/groceries/')
        resp = json.loads(r.content.decode())
        groceries = resp['items']
        self.assertEqual(groceries[7]['price'], '4.59')

if __name__ == '__main__':
    unittest.main()